require 'rails_helper'

RSpec.describe Board, type: :model do
  let(:user) { create(:user) }
  let(:board_private) { build(:board, public: false) }
  let(:board_public) { build(:board, public: true) }
  
  it { is_expected.not_to be_valid }
  it { is_expected.to validate_presence_of :title }
  it { is_expected.to belong_to :user }
  it { is_expected.to have_many :tasks }
  it { is_expected.to respond_to("public?") }

  it 'should has a user_id attribute when board is private' do
    board_private.valid?
    expect(board_private.errors[:user_id].size).to eq(1)
  end
  it 'should be valid when board be public' do
    expect(board_public.valid?).to be_truthy
  end

  context 'scope' do
    it 'all_public' do
      expect { create(:board, public: true, user: user) }.to change{ Board.all_public.count }.from(0).to(1)
    end
    it 'all_private' do
      expect { create(:board, public: false, user: user) }.to change{ Board.all_private.count }.from(0).to(1)
    end
  end
end
