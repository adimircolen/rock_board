require 'rails_helper'

RSpec.describe Task, type: :model do
  let(:user) { create(:user) }
  let(:board) { create(:board, user: user) }
  let(:attr_task) { attributes_for(:task) }
  let(:statuses) { [ :to_do, :in_progress, :to_verify, :done ] }
  let(:priorities) { [ :low, :normal, :high ] }
  it { is_expected.not_to be_valid }
  it { is_expected.to validate_presence_of :title }
  it { is_expected.to validate_presence_of :board_id }
  it { is_expected.to belong_to :user }
  it { is_expected.to belong_to :board }

  it 'has the status index right' do
    statuses.each_with_index do |item, index|
      expect(described_class.statuses[item]).to eq index
    end
  end
  
  it 'has the priority index right' do
    priorities.each_with_index do |item, index|
      expect(described_class.priorities[item]).to eq index
    end
  end
  
  it 'respond to status methods' do
    statuses.each do |status|
      expect(Task.new).to respond_to("#{status.to_s}?", "#{status.to_s}!")
    end
  end
  
  it 'respond to priority methods' do
    priorities.each do |priority|
      expect(Task.new).to respond_to("#{priority.to_s}?", "#{priority.to_s}!")
    end
  end

end
