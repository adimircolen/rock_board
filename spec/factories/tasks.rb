FactoryGirl.define do
  factory :task do
    title "MyString"
    priority 1
    status 1
    user nil
    board nil
  end
end
