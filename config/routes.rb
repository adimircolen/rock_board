Rails.application.routes.draw do
  devise_for :users
  resources :boards do
    resources :tasks
  end
  root 'boards#index'
end
