class BoardsController < ApplicationController
  before_action :set_board, only: [:show, :edit, :update, :destroy]
  def index
    @boards = Board.all_public
    @boards += Board.by_user_and_private(current_user) if user_signed_in?
  end

  def new
    @board = Board.new
  end

  def show
  end

  def create
    @board = Board.new(board_params)
    @board.user = current_user if user_signed_in?
    
    if @board.save
      redirect_to @board, notice: 'Board created successfully!'
    else
      render :new
    end
    
  end
  
  def destroy
    @board.destroy
    redirect_to boards_url, notice: 'Board deleted successfully!'
  end

  private
    def set_user
      
    end

    def set_board
      @board = Board.find(params[:id])
    end

    def board_params
      params.require(:board).permit(:title, :public, :user_id)
    end
end