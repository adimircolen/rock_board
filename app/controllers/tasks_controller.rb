class TasksController < ApplicationController
  before_action :set_board
  
  def index
    @tasks = @board.tasks
  end
  
  def new
    @task = Task.new
  end
  
  def create
    @task = @board.tasks.new(task_params)
    @task.user = current_user if user_signed_in?
    
    respond_to do |format|
      if @task.save
        format.html { redirect_to @board, notice: 'Task created successfully!' }
        format.js { render json: @task, status: :created }
      else
        format.html { render 'new' }
        format.js { render json: @task.errors.full_messages, status: :unprocessable_entity }
      end
    end
  end
  
  private
    def set_board
      @board = Board.find(params[:board_id])
    end
    
    def task_params
      params.require(:task).permit(:title, :user_id, :priority, :status)
    end
end