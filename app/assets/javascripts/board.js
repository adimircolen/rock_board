$(function() {
  $("#newTask").on("ajax:success", function(event, data, status, xhr) {
    var task;
    console.log(data);
    // $('.lanes').find('.' + data.status)
    $('#error_explanation').hide();
    $('#newTask').modal('hide');
  });
  $("#newTask").on("ajax:error", function(event, xhr, status, error) {
    var e, errorcount, errors, i, len;
    errors = jQuery.parseJSON(xhr.responseText);
    errorcount = errors.length;
    $('#error_explanation').empty();
    if (errorcount > 1) {
      $('#error_explanation').append('<div class="alert alert-danger">O Formulario contem ' + errorcount + ' erros.</div>');
    } else {
      $('#error_explanation').append('<div class="alert alert-danger">O Formulario contem 1 error</div>');
    }
    $('#error_explanation').append('<ul>');
    for (i = 0, len = errors.length; i < len; i++) {
      e = errors[i];
      $('#error_explanation').append('<li>' + e + '</li>');
    }
    $('#error_explanation').append('</ul>');
    $('#error_explanation').show();
  });
});