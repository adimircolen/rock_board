class Task < ActiveRecord::Base
  belongs_to :user
  belongs_to :board
  validates :title, :board_id, presence: true
  enum status: [ :to_do, :in_progress, :to_verify, :done ]
  enum priority: [ :low, :normal, :high ]
end
