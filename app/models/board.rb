class Board < ActiveRecord::Base
  belongs_to :user
  has_many :tasks
  validates :title, presence: true
  validates :user_id, presence: true, unless: "public?"
  
  # Scope
  scope :all_public, -> { where(public: true) }
  scope :all_private, -> { where(public: false) }
  scope :by_user_and_private, -> (user) { where(user_id: user, public: false) }
end
